app.controller('shoppingList', ['$scope', 'shoppingCart', 'serviceHandler', '$log', '$rootScope', '$timeout', 'global',
    function ($scope, shoppingCart, serviceHandler, $log, $rootScope, $timeout, global) {

        $rootScope.currentPage = 'shoppingList';

        var passedData = $rootScope.passedData;
        if (passedData && passedData.catId) {
            getProductList(passedData.catId);
        } else {
            getProductList('recommended');
        }

        function getProductList(catId) {
            global.showLoadingDialog().then(function (dialog) {
                var url = servicesConfiguration.shoppingListBaseUrl + catId;
                serviceHandler.runService(url).then(function (response) {
                    if (response && response.data && response.data.length > 0) {
                        $scope.data = response.data;
                        $log.debug($scope.data);
                        dialog.hide();
                    }
                }, function (error) {
                    $log.error('error: ', error);
                    dialog.hide();
                });

            });
        }


        $scope.onShoppingCartClicked = function () {
            if (Object.keys(shoppingCart.shoppingCartList).length > 0) {
                $rootScope.passedData = {shoppingCartList: shoppingCart.shoppingCartList};
                $rootScope.navigateView('shoppingCart', 1);
            }

        }

    }]);