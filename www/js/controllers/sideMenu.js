app.controller('sideMenu', function ($scope, $rootScope, $timeout) {

    $rootScope.toggleAppMenu = function () {
        $timeout(function () {
            if (app.slidingMenu) {
                if (app.slidingMenu.isMenuOpened()) {
                    app.slidingMenu.closeMenu();
                } else {
                    app.slidingMenu.openMenu();
                }
            }
        });
    };
});