app.controller('shoppingListCategories', function ($scope, $rootScope, global, serviceHandler) {
    function getCategories() {
        global.showLoadingDialog().then(function (dialog) {
            var url = servicesConfiguration.shoppingListBaseUrl + 'categories';
            serviceHandler.runService(url).then(function (response) {
                if (response && response.data && response.data.length > 0) {
                    $scope.categories = response.data;
                    dialog.hide();
                }
            }, function (error) {
                dialog.hide();
            });

        });
    }

    getCategories();

    $scope.getCategoryItems = function (category) {
        $rootScope.toggleAppMenu();
        $rootScope.passedData = {catId: category.id};
        $rootScope.navigateView('shoppingList', 1);
    }

});