app.factory('serviceHandler', ['$http', '$q', function ($http, $q) {

    return {
        runService: function (serviceUrl, data) {
            var defer = $q.defer();
            $http.get(serviceUrl, {params: data}).then(function (response) {
                defer.resolve(response);
            }, function (error) {
                defer.reject(error);
            })

            return defer.promise;
        }
    }

}]);