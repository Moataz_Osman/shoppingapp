app.factory('global', function ($q) {

    var showLoadingDialog = function(cancellable) {
        var defer = $q.defer();
        // var $this = this;
        ons.createDialog("templates/loadingDialog.html").then(function (loadingDialog) {
            loadingDialog.id = (new Date()).getTime();
            loadingDialog.cancellable = angular.isDefined(cancellable) && cancellable != null ? cancellable : false;
            loadingDialog.show();
            // $this.show(loadingDialog);
            defer.resolve(loadingDialog);
        }, function (error) {
            defer.reject(error);
        });
        return defer.promise;
    };

    return {
        showLoadingDialog: showLoadingDialog
    }

});