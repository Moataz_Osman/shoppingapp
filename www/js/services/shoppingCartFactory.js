app.factory('shoppingCart', ['$rootScope', function ($rootScope) {

    return {
        shoppingCartList: {},
        listeners: [],
        badgeNumber: 0,
        totalAmount: 0,
        addToShoppingCart: function (item, itemCount) {
            var $this = this;
            item['itemCount'] = itemCount;
            $this.shoppingCartList[item.name] = item;
            $this.totalAmount += item.price * itemCount;
            $this.setBadgeNumber();
        },
        deleteFromShoppingCart: function (item) {
            var $this = this;
            $this.totalAmount -= item.price * item.itemCount;
            delete $this.shoppingCartList[item.name];
            $this.setBadgeNumber();
        },
        registerListener: function ($scope, fn) {
            var $this = this;
            $this.listeners.push({scope: $scope, fun: fn})
        },
        notifyListeners: function (event, value) {
            var $this = this;
            for (var i = 0; i < $this.listeners.length; i++) {
                $this.listeners[i]['fun'](event, value);
            }
            // $this.setBadgeNumber();
        },
        setBadgeNumber: function () {
            var $this = this;
            $this.badgeNumber = Object.keys($this.shoppingCartList).length;
            $this.notifyListeners();
        }
    }
}]);