app.directive('accordion', function () {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        template: '<div ng-transclude=""></div>',
        controller: function ($scope, $element, $attrs) {

            var accordionItems = [];
            this.open = function (selectedItem) {
                angular.forEach(accordionItems, function (accordionItem) {
                    if (selectedItem.$id != accordionItem.$id)
                        accordionItem.showMe = false;
                });
            };
            this.addAccordionItem = function (accordionItem) {
                accordionItems.push(accordionItem);
            };
        },
        link: function (scope, element, attrs) {
        }
    }
});