app.controller('shoppingCart', ['$scope', 'shoppingCart', 'serviceHandler', '$log', '$rootScope', function ($scope, shoppingCart, serviceHandler, $log, $rootScope) {

    var passedData = $rootScope.passedData;
    if (passedData && passedData.shoppingCartList) {
        $scope.shoppingCartList = passedData.shoppingCartList;
        $scope.totalAmount = shoppingCart.totalAmount;
        $scope.getTotalAmount = function () {
            $scope.totalAmount = shoppingCart.totalAmount;
        };

        shoppingCart.registerListener($scope, $scope.getTotalAmount);
    }
}]);