app.directive('accordionItem', function () {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        require: '^accordion',
        scope: {},
        templateUrl: 'templates/accordion_item.html',
        controller: function ($scope, $element, $attrs) {

        },
        link: function (scope, element, attrs, accordion) {
            scope.showMe = false;
            accordion.addAccordionItem(scope);
            scope.toggle = function () {
                scope.showMe = !scope.showMe;
                accordion.open(scope);
            };

            attrs.$observe('title', function (title) {
                if (angular.isDefined(title) && title != '') {
                    scope.title = title;
                }
            });
        }
    }
});