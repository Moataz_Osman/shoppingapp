app.directive('header', ['$rootScope', 'shoppingCart', function ($rootScope, shoppingCart) {
    return {
        restrict: 'E',
        scope: {
            onShoppingCartClicked: "&?onShoppingCartClicked"
        },
        templateUrl: 'views/header.html',
        link: function (scope, element, attrs) {

            scope.headerTitle = $rootScope.getPageObj($rootScope.currentPage).headerTitle;
            scope.badgeNumber = shoppingCart.badgeNumber;
            scope.getBadgeNumber = function () {

                scope.badgeNumber = shoppingCart.badgeNumber;

            };
            shoppingCart.registerListener(scope, scope.getBadgeNumber);

            var onShoppingCartClicked = scope.onShoppingCartClicked ? scope.onShoppingCartClicked : undefined;

            scope.onShoppingCartClickedFunction = function () {
                if (angular.isDefined(onShoppingCartClicked)) {
                    onShoppingCartClicked()();
                }
            };

            scope.openSlidingMenu = function () {
                $rootScope.toggleAppMenu();
            }

        }
    }
}]);