app.directive('product', ['$log', 'shoppingCart', function ($log, shoppingCart) {
    return {
        restrict: 'E',
        scope: {
            data: '=',
            comeFrom: '@?'
        },
        templateUrl: 'views/product.html',
        link: function (scope, element, attrs) {
            scope.product = angular.copy(scope.data);
            scope.productCount = 1;

            scope.comeFrom === 'shoppingList' ? scope.comeFromShoppingList = true : scope.comeFromShoppingCart = true;
            // var onProductClicked = scope.onProductClicked ? scope.onProductClicked : undefined;

            // scope.onProductClickedFunction = function () {
            //     if (angular.isDefined(onProductClicked)) {
            //         onProductClicked()(scope.data, scope.productCount);
            //     }
            // };

            scope.addToShoppingCart = function () {
                shoppingCart.addToShoppingCart(scope.data, scope.productCount);
            };

            scope.increaseCount = function (productCount) {
                scope.productCount = ++productCount;
                refreshProductPrice(scope.data, productCount);
            };

            scope.decreaseCount = function (productCount) {

                if (productCount > 1) {
                    scope.productCount = --productCount;
                    refreshProductPrice(scope.data, productCount);
                }
            };

            var refreshProductPrice = function (product) {
                scope.product.price = product.price * scope.productCount;
            };

            scope.deleteFromShoppingCart = function () {
                shoppingCart.deleteFromShoppingCart(scope.data);
            }
        }
    }
}]);