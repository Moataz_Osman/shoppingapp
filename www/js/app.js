var dependencies = ['onsen'];
// var app = angular.module('shoppingApp', dependencies);
var app = ons.bootstrap('starter', dependencies);
app.run(['$rootScope', '$timeout', function ($rootScope, $timeout) {

    ons.ready(function () {


        $rootScope.appPages = [
            {
                tag: "sideMenu",
                viewPath: "templates/side_menu.html"
            },
            {
                tag: "shoppingList",
                viewPath: "views/shoppingList.html",
                headerTitle: "Shopping List"
            },
            {
                tag: 'shoppingCart',
                viewPath: "views/shoppingCart.html",
                headerTitle: "Shopping Cart"
            }
        ];


        $rootScope.getPageObj = function (tag) {
            $rootScope.currentPage = tag;
            console.log('current tag ' + $rootScope.currentPage);
            var item = {};
            for (var id = 0; id < $rootScope.appPages.length; id++) {
                item = $rootScope.appPages[id];
                if (tag === item.tag) {
                    return item;
                }
            }
            return {};
        };

        $rootScope.navigateView = function (tag, type, data) {
            if (type === 1) {
                if ($rootScope.currentPage !== 'tag') {
                    var item = $rootScope.getPageObj(tag);
                    app.navi.pushPage(item.viewPath, {data: data});
                    $rootScope.currentPage = tag;
                }
            }

        };

        $rootScope.toggleAppMenu = function () {
            $timeout(function () {
                if (app.slidingMenu) {
                    if (app.slidingMenu.isMenuOpened()) {
                        app.slidingMenu.closeMenu();
                    } else {
                        app.slidingMenu.openMenu();
                    }
                }
            });
        };

        // $rootScope.navigateView('splash', 1);
        if (window.navigator && window.navigator.splashscreen) {
            navigator.splashscreen.hide();
        }
        $rootScope.navigateView('sideMenu', 1);


    })
}]);